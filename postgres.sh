#!/bin/bash

# Premiere fonction : Installation et configuration de PostgreSQL
fonction_1() {

echo "*************Installation et Configuration postgresql****************"

# Installation
sudo apt update
sudo apt install postgresql postgresql-contrib -y

# Remplacement la ligne dans le fichier
sudo sed -i "s/^#listen_addresses = 'localhost'/listen_addresses = '$IP'/" "$config_file"

# Vérifier si la modification a été effectuée avec succès
if [ $? -eq 0 ]; then
    echo "La configuration de listen_addresses a été modifiée avec succès."
else
    echo "Erreur lors de la modification de la configuration de listen_addresses."
fi

# Autorisation de prt pour accéder à la base
if sudo grep -qF "$line" "$hba_conf_file"; then
    echo "La ligne existe déjà dans le fichier. Aucune action nécessaire."
else
    sudo sed -i "/^# \"local\" is for Unix domain socket connections only/a $line" "$hba_conf_file"
    # Vérifier si la modification a été effectuée avec succès
    if [ $? -eq 0 ]; then
        echo "La configuration de l'autorisation de prt a été modifiée avec succès."
    else
        echo "Erreur lors de l'autorisation de prt."
    fi
fi

# Redemarrage du service postgres
sudo systemctl daemon-reload
sudo systemctl restart postgresql

# Création d'utilisateur prt, de la base dbprt et du schéma EXP
sudo su - postgres<<FINPOSTGRES

# Vérifier si l'utilisateur 'prt' existe
psql -tAc "SELECT 1 FROM pg_roles WHERE rolname = 'prt'" | grep -q 1
if [ $? -eq 0 ]; then
    echo "L'utilisateur 'prt' existe déjà."
else
    psql -c "CREATE USER prt WITH PASSWORD 'prt' CREATEDB;"
    psql -c "ALTER USER prt WITH SUPERUSER;"
    echo "L'utilisateur 'prt' a été créé avec succès."
fi

# Vérifier si la base de données 'bdprt' existe
psql -tAc "SELECT 1 FROM pg_database WHERE datname = 'bdprt'" | grep -q 1
if [ $? -eq 0 ]; then
    echo "La base de données 'bdprt' existe déjà."
else
    echo "La base de données 'bdprt' n'existe pas. Création en cours..."
    psql -c "CREATE DATABASE bdprt;"
    echo "La base de données 'bdprt' a été créée avec succès."
fi

    psql -d bdprt -U prt<<FINPSQL

        CREATE SCHEMA IF NOT EXISTS EXP;
        CREATE TABLE IF NOT EXISTS EXP.facture (
            id SERIAL PRIMARY KEY,
            prix DECIMAL
        );
        
        CREATE TABLE IF NOT EXISTS EXP.client (
            id SERIAL PRIMARY KEY,
            nom VARCHAR(255),
            prenom VARCHAR(255),
            ville VARCHAR(255)
        );

        INSERT INTO EXP.facture (prix) VALUES (100.50), (200.75), (300.25);
        INSERT INTO EXP.client (nom, prenom, ville) VALUES ('SAMB', 'Abdou', 'Tamba'), ('Mouhamed', 'Mouhamed', 'Meq'), ('AAS', 'AAS', 'London');


        GRANT ALL PRIVILEGES ON SCHEMA EXP TO prt;

FINPSQL

FINPOSTGRES

}


# Exportation du schema EXP
fonction_2() {
echo "*************Exportation de schema****************"

sudo su - postgres<<FINPOSTGRES

    # Vérifier si le dossier backup existe et le créer si non
    if [ ! -d "$backup_dir" ]; then
        mkdir "$backup_dir"
    fi

    # Verifier si le shema EXP existe et l'exporter
    # Exportation du shema EXP
    psql -d bdprt -tAc "SELECT 1 FROM pg_namespace WHERE nspname = 'EXP'" | grep -q 1
    if [ $? -eq 0 ]; then
        pg_dump -U prt -F p -n EXP -f ./$backup_dir/$backup_file bdprt
        echo "Schema EXP exporté vers $backup_dir/$backup_file"
    else
        echo "Le schéma 'EXP' n'existe pas dans la base de données 'bdprt'."
    fi

FINPOSTGRES
}

# Importation du schema EXP
fonction_3() {
echo "*************Importation de schema****************"

sudo su - postgres<<FINPOSTGRES

    # Vérifier si la base de données 'dbofi' existe
    psql -tAc "SELECT 1 FROM pg_database WHERE datname = 'dbofi'" | grep -q 1
    if [ $? -eq 0 ]; then
        echo "La base de données 'dbofi' existe déjà."
    else
        echo "La base de données 'dbofi' n'existe pas. Création en cours..."
        sudo -u postgres psql -c "CREATE DATABASE dbofi;"
        echo "La base de données 'dbofi' a été créée avec succès."
    fi


    # Importer le schéma si la base de données existe
    if psql -d dbofi -tAc "SELECT 1 FROM information_schema.tables WHERE table_schema = 'exp' AND table_name = 'client'" | grep -q 1; then
        echo "Le schéma 'exp' et ses tables existent déjà dans la base de données 'dbofi'. Aucune action nécessaire."
    else
        # Importer le schéma si la base de données existe
        psql -d dbofi -U postgres -f ./$backup_dir/$backup_file
        echo "Le schéma EXP a été importé depuis $backup_dir/$backup_file."
    fi

FINPOSTGRES
}


# Script main
if [ $# -ne 1 ]; then
    echo "Le nombre de parametres est incorrect"
    exit 1
fi

# Récupéerer l'action à réaliser
action="$1"

# Définir les variables
config_file="/etc/postgresql/14/main/postgresql.conf"
hba_conf_file="/etc/postgresql/14/main/pg_hba.conf"
IP="*"
backup_file="backup_schema_EXP.sql"
backup_dir="backup_exo5"
line="local    bdprt           prt                                    trust"

# Appel de la fonction concernée
if [ $action == "inst_conf" ]; then
    fonction_1 "$config_file" "$hba_conf_file" "$line"

elif [ $action == "exporter" ]; then
    fonction_2 "$backup_dir" "$backup_file"

elif [ $action == "importer" ]; then
    fonction_3 "$backup_dir" "$backup_file"

elif [ $action == "all" ]; then
    fonction_1 "$config_file" "$hba_conf_file" "$line"
    fonction_2 "$backup_dir" "$backup_file"
    fonction_3 "$backup_dir" "$backup_file"

else
    echo "Argument non valide."
    exit 1
fi
